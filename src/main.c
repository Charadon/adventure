#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <main_variables.h>

//Beginning of Gameplay/Text related functions

//POSIX Compliant clear screen.
void clear_screen(){
	printf("\033[2J\033[1;1H");
	return;
}

//Consume Newlines thus fixing bug #1 and #2. This function must be called after EVERY scanf!
//Massive thanks to this forum post which solved the issue for me. https://bytes.com/topic/c/answers/220232-new-line-character-scanf-getchar
void consume_newline(){
	while((getchar()) != '\n');
	return;
}

//Checks health, this function should be called before EVERY function, period.
void check_health(){
	if (health < 1){
		clear_screen();
		printf("\033[0;31mYou are died.");
		getchar();
		exit(0);
	}
}

//End of gameplay/text related functions.

//Beginning of Game.
int main(){
        printf("Welcome to Adventure.\nThis program is licensed under the GPL-3 <www.gnu.org/licenses/gpl-3.0.txt>\nCreated by Charadon <iotib.net>\nCurrent Bugs: \n\tNone outside of incomplete functions.");
        getchar();
        clear_screen();
        character_creation();
        return 0;
}
