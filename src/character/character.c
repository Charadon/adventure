#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <main_variables.h>

//Beginning of Functions:

//Shows Stats
void show_stats(){
    //Listing Attributes
    printf("\nCurrent Stats:\n\tHealth: %i Speech: %i Speed: %i Strength: %i\n\tAgility: %i Endurance: %i Intelligence: %i\nPerks:\n\t", health, speech, speed, strength, agility, endurance, intelligence);
    //Listing Perks
    if (crafty == 1){
        printf("Crafty, ");
    }
    if (martial_artist == 1){
        printf("Martial Artist, ");
    }
    if (scout == 1){
        printf("Scout, ");
    }
    if (peddler == 1){
        printf("Peddler, ");
    }
    if (magic_user == 1){
        printf("Magic User, ");
    }
    if (wild_world == 1){
        printf("Wild World, ");
    }
	//Listing Inventory Items

    printf("\nInventory:\n\tGold: %i, ", gold);
	if (whoopie_cushion == 1){
		printf("Whoopie Cushion, ");
	}
	if (confetti_cannon == 1){
		printf("Confetti Cannon, ");
	}
	if (stink_bomb == 1){
		printf("Stink Bomb, ");
	}
	if (health_potion == 1){
        printf("Healh Potion, ");
    }
	printf("\n");
    getchar();
    showing_stats = 1;
}

//Character Creation
void character_creation(){
	//Character Creation: This handles name selection.
	check_health();
	printf("To begin this adventure, what is your name? (Limited to 20 Characters): ");
	scanf("%s", name);
	consume_newline();
	childhood();
}
void childhood(){
	check_health();
	//Easter Eggs
	char name_easter_egg_charadon[20] = "Charadon";
	char name_easter_egg_cody[20] = "Cody"; 
	char name_debug[20] = "debug";
	if (strcmp(name_easter_egg_charadon, name) == 0){
		printf("That's a pretty gay name.");
		getchar();
	}
	if (strcmp(name_easter_egg_cody, name) == 0){
		printf("That's a weird name.");
		getchar();
	}
	if (strcmp(name_debug, name) == 0){
		debug_gold();
		return;
	}
	//This will be about childhood, and depending on your answers, will determine what your stats are.
	choice = 0;
	failed = 0;
	clear_screen();
	printf("Greetings %s, to make sure you are who you say you are, i'll ask some questions about your childhood.\n", name);
	getchar();
	childhood_question_one();
	return;
}

void childhood_question_one(){
	check_health();
    // Question 1 (Health)
    if (failed == 0){
		clear_screen();
        printf("How healthy were you as a boy:\n[1]I've never been sick my entire life. (Easy)\n[2]I got sick every once and a while. (Normal)\n[3]Was sick all the time. (Hard)\nEnter Number: ");
        scanf("%c", &choice);
		consume_newline();
    }
    else{
        failed = 0;
        printf("What? I ask again, how healthy were you?: ");
        scanf("%c", &choice);
		consume_newline();
    }
    switch(choice){
        case '1':
            health += 50;
            printf("\nGot a good set of genes, eh?\n");
            getchar();
            choice = 0;
            failed = 0;
            childhood_question_two();
            return;
        case '2':
            printf("\nSounds typical.\n");
            getchar();
            choice = 0;
            failed = 0;
            childhood_question_two();
            return;
        case '3':
            health -= 50;
            printf("\nI wonder how you are still alive?\n");
            getchar();
            choice = 0;
            failed = 0;
            childhood_question_two();
            return;
        default:
            failed = 1;
            childhood_question_one();
            return;
    }
}

void childhood_question_two(){
	check_health();
    //Question 2
    if (failed == 0){
		clear_screen();
        printf("Next question, how did you get out of hostile situations as a kid?:\n[1]Talked my way out of them\n[2]I bargained.\n[3]Ran away.\n[4]Took it head-on!\nEnter Number: ");
        scanf("%c", &choice);
		consume_newline();
    }
    else{
        failed = 0;
        printf("That didn't answer the question, try again: ");
        scanf("%c", &choice);
		consume_newline();
    }
    switch(choice){
        case '1':
            speech += 5;
            strength -= 2;
            intelligence += 1;
            printf("\nFight with words and not fists, eh?\n");
            getchar();
            choice = 0;
            failed = 0;
            childhood_question_three();
            return;
        case '2':
            speech += 1;
            strength -=1;
            intelligence += 5;
            //placeholder
            printf("\nThe old tribute method, huh?\n");
            getchar();
            choice = 0;
            failed = 0;
            childhood_question_three();
            return;
        case '3':
            speech -= 3;
            strength -= 3;
            intelligence += 2;
            speed += 7;
            printf("\nSometimes, the cowards survive at the end of things.\n");
            getchar();
            choice = 0;
            failed = 0;
            childhood_question_three();
            return;
        case '4':
            speech -= 5;
            strength += 7;
            endurance += 3;
            intelligence -=2;
            printf("\nI guess people can't hurt you if you hurt them first. Haha.\n");
            getchar();
            choice = 0;
            failed = 0;
            childhood_question_three();
            return;
        default:
            failed = 1;
            childhood_question_two();
            return;
    }
}

void childhood_question_three(){
	check_health();
    //Question 3
    if (failed == 0){
		clear_screen();
        printf("Okay, one last question, when you were but a child, how did you spend your freetime?:\n[1]Practicing Martial Arts\n[2]Socializing with friends and peers.\n[3]Read a lot of books.\n[4]Bullied other children.\nEnter Number: ");
        scanf("%c", &choice);
		consume_newline();
    }
    else{
        failed = 0;
        printf("That doesn't sound right, say again?: ");
        scanf("%c", &choice);
		consume_newline();
    }
    switch(choice){
        case '1':
            speed += 5;
            agility += 7;
            strength += 1;
            endurance += 1;
            printf("\nAh yes, the art of combat.\n");
            getchar();
            failed = 0;
            childhood_over();
            return;
        case '2':
            speech += 8;
            intelligence += 4;
            printf("\nNothing like hanging out with the boys at 2am looking for beans.\n");
            getchar();
            failed = 0;
            childhood_over();
            return;
        case '3':
            intelligence += 10;
            speed += 1;
            speech += 1;
            strength += 1;
            agility += 1;
            endurance += 1;
            printf("\nYou are indeed very book smart, but I wonder if that will translate to street smart?\n");
            getchar();
            failed = 0;
            childhood_over();
            return;
        case '4':
            strength += 8;
            endurance += 8;
            speed += 2;
            printf("\nMean little shit, weren't you?\n");
            getchar();
            failed = 0;
            childhood_over();
            return;
        default:
            failed = 1;
            childhood_question_three();
            return;
    }
}

void childhood_over(){
	check_health();
    // Starting Perks
    if (failed == 0){
		clear_screen();
        printf("Yep, you are definitely you alright.\nGenerated Stats:\nName: %s\nHealth: %i\nSpeech: %i\nSpeed: %i\nStrength: %i\nAgility: %i\nEndurance: %i\nIntelligence: %i\nPress enter key to continue.", name, health, speech, speed, strength, agility, endurance, intelligence);
        getchar();
		clear_screen();
        printf("\nNow, what is your trade?:\n[1]Blacksmith\n[2]Professional Fighter\n[3]Wanderer\n[4]Merchant\n[5]Apprentice Wizard\n[6]Conspiracy Theorist\nEnter Number: ");
        scanf("%c", &choice);
		consume_newline();
    }
    else{
        printf("Excuse me, come again?: ");
        failed = 0;
        scanf("%c", &choice);
		consume_newline();
    }
    switch(choice){
        case '1':
            crafty = 1;
            printf("Crafty Perk Unlocked.\n");
            getchar();
            prologue_();
            return;
        case '2':
            martial_artist = 1;
            printf("Martial Artist Perk Unlocked.\n");
            getchar();
            prologue_();
            return;
        case '3':
            scout = 1;
            printf("Scout Perk Unlocked.");
            getchar();
            prologue_();
            return;
        case '4':
            peddler = 1;
            printf("Goods Peddler Perk Unlocked.");
            getchar();
            prologue_();
            return;
        case '5':
            magic_user = 1;
            printf("Magic User Perk Unlocked.");
            getchar();
            prologue_();
            return;
        case '6':
            wild_world = 1;
            printf("Wild World Perk Unlocked.");
            getchar();
            prologue_();
            return;
        default:
            failed = 1;
            childhood_over();
            return;
    }
}

//Debug Functions
void debug_gold(){
    printf("\nPoor or Rich?\nChoice: ");
        scanf("%c", &choice);
        consume_newline();
        switch(toupper(choice)){
            case 'P':
                gold = 0;
                debug_attributes();
                return;
            case 'R':
                gold = 9999;
                debug_attributes();
                return;
            default:
                exit(0);
                return;
        }
}

void debug_attributes(){
    printf("\nGod, or Worm?\nChoice: ");
    scanf("%c", &choice);
    consume_newline();
    switch(toupper(choice)){
        case 'G':
            //Sets all Attributes to 9999
                health = 9999;
                speech = 9999;
                speed = 9999;
                strength = 9999;
                agility = 9999;
                endurance = 9999;
                intelligence = 9999;
            //Unlocks all Perks
                crafty = 1;
                martial_artist = 1;
                scout = 1;
                peddler = 1;
                magic_user = 1;
                wild_world = 1;
            debug_area();
            case 'W':
                //Sets all Attributes to 0 except health, otherwise the game would end.
                    health = 1;
                    speech = 0;
                    speed = 0;
                    strength = 0;
                    agility = 0;
                    endurance = 0;
                    intelligence = 0;
                //Locks all Perks
                    crafty = 0;
                    martial_artist = 0;
                    scout = 0;
                    peddler = 0;
                    magic_user = 0;
                    wild_world = 0;
                debug_area();
                default:
                    exit(0);
                    return;
            }
}

void debug_area(){
    //Finally, goes to area I want to debug.
        jokli_town();
        return;    
}
