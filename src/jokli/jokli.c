#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <main_variables.h>

//Prologue and Starting hub for jokli. Potentially a place to cut out?
void prologue_(){
	check_health();
	clear_screen();
	printf("\n\nPrologue:\nYou find yourself in the Republic of Okai, currently locked in a bloody civil war between the Republican Jekars and the Extremely Zealot Yuikinai Cult. You're goal is to get the hell out of this country and get to the Kingdom of Stresin to the east. You are rougly %i%% the way there, and are currently in the city of Jokli which is currently untouched by the war. As you take in the sights, you must decide what you will do first.\n", distance);
	getchar();
	jokli_town();
	return;
}

//General hub for jokli.
void jokli_town(){
	check_health();
    if (failed == 0){
        if (showing_stats == 0){
            //Non-perk decisions.
		clear_screen();
            printf("What will you do now?:\n[0]Show Stats\n[1]Talk to some locals\n[2]Go to a local Tavern/Inn\n[3]Go to shop\n");
            //Perk Decisions.
            if (scout == 1){
            printf("[4]Scout around (Scout)");
            }
        }
        printf("\nEnter Number: ");
        showing_stats = 0;
        scanf("%c", &choice);
		consume_newline();
    }
    else{
        failed = 0;
        printf("\nTry again: ");
        scanf("%c", &choice);
		consume_newline();
    }
	switch (choice){
		case '0':
			show_stats();
			jokli_town();
			return;
		case '1':
			if (talked_to_jokli_locals == 1){
				printf("You've already talked to the locals.");
				getchar();
				failed = 1;
				jokli_town();
				return;
			}
			else{
				talking_to_locals_jokli();
				return;
			}
		case '2':
			if (been_to_tavern_jokli == 1){
				printf("You've already been to the tavern.");
				getchar();
				failed = 1;
				jokli_town();
				return;
			}
			else{
				jokli_tavern();
				return;
			}
		case '3':
			if (been_to_store_jokli == 1){
				jokli_shop();
				return;
			}
			else{
				jokli_shop();
				return;
			}
		case '4':
            if (scout == 1){
			if (already_scouted_jokli == 1){
				printf("You've already scouted the town.");
				failed = 1;
				jokli_town();
				return;
			}
			else{
                		jokli_scout();
                		return;
			}
            }
            else{
                failed = 1;
                jokli_town();
                return;
            }
		default:
			failed = 1;
			jokli_town();
			return;
	}
}

//Talking to the locals
	void talking_to_locals_jokli(){
		check_health();
        if (failed == 0){
			//Non-perk Decisions
			clear_screen();
            printf("You try to talk to some of the locals, but most ignore you. Finally, right when you are about to give up, someone taps your shoulder. You turn around and see a woman. \"You okay, you're running around like a chicken with its head cut off.\"");
			getchar();
			printf("\n[1]Oh, yes, I was wondering what the word in town was.\n[2]Ay baby, let's DO IT! \n[3]Who are you?\nYou say: ");
			//Perk Decisions
            if (peddler == 1){
                printf("[4]Yes, i'm fine. Hey, i'm kinda on a budget, know any good stores?");
            }
		scanf("%c", &choice);
		consume_newline();
        }
        else{
            printf("She stares at you blankly as if she has no idea what you are talking about.\nReword: ");
            scanf("%c", &choice);
			consume_newline();
        }
        switch(choice){
        case '1':
			clear_screen();
			printf("\"I'm sure you've heard of the war going on, well... rumor has it that The Cult is going to begin new Campaign, and Jokli is going to be in the middle of it.\" Her expression turns to an extremely worried look. \"Me and my daughter are planning on leaving and going north to where my brother lives, he lives in the mountains far away from this mess.\" She sighs. \"Well, I got to finish shopping, maybe i'll see you around.\" You part ways with each other. You reflect on what you've heard... a new campaign? You need to get out of this town!");
			getchar();
			talked_to_jokli_locals = 1;
			jokli_town();
			return;
		case '2':
			clear_screen();
			printf("Her expression goes from concerned, to extremely unamused. Before you know it, you feel a extremely sharp pain on your cheek. You got slapped!");
			getchar();
			printf("\nYou lose 5HP!\n");
			getchar();
			printf("Maybe next time your brain should do the talking.");
			getchar();
			health -= 5;
			talked_to_jokli_locals = 1;
			jokli_town();
			return;
		case '3':
			clear_screen();
			printf("\"Oh, my name is Karisa, nice to meet you. What's your name?\" You tell her your name is %s. \"What a nice name. What brings you to town?\" You explain that you are trying to get out of the country and away from the war. \"Same here, me and my daughter are going to move up north with my brother. He lives in the mountains and is far away from the war. I wish you the best %s.\" She reaches into a basket that you just noticed she was holding. \"Here, take this apple, just got it from the market. Enjoy it!\" She hands you the apple, and with a final wave, she leaves your company. What a nice woman.\n", name, name);
			getchar();
			printf("You eat the apple and gain 1 endurance.\n");
			getchar();
			talked_to_jokli_locals = 1;
			jokli_town();
			return;
		case '4':
			printf("\"Budget shops? Hmmm\" She thinks for a moment. \"One of my friends goes on and on about Kakles prices. He's on the south part of town\" You thank her and part ways.");
			getchar();
			printf("You have gained knowledge of Kakles store");
			getchar();
			knowledge_of_kakles = 1;
			talked_to_jokli_locals = 1;
			jokli_town();
			return;
		default:
			failed = 1;
			talking_to_locals_jokli();
			return;
        }
    }

//Tavern in jokli.
void jokli_tavern(){
		check_health();
		if (been_to_tavern_jokli == 0){

		}
}

//Shop in jokli
void jokli_shop(){
	check_health();
	clear_screen();
	if (failed == 0){
		if (been_to_store_jokli == 0){
			printf("You walk into the first store you see called \"The Jokli General Store\". You walk into the store and are greeted by the shopkeeper who asks what you want. You ask for a:\n[1]Health Potion\n[2]Shield\n[3]Sword");
		}
		else{
			printf("You walk into the Jokli General Store again. The shopkeeper remembers you and says \"Welcome back! Waddaya need?\"\nYou ask for a:\n[1]Health Potion\n[2]Shield\n[3]Sword");
		}
		printf("\nYour Choice: ");
		scanf("%c", &choice);
		consume_newline();
	}
	else{
		printf("The shopkeeper looks confused by your answer. \"Sorry, come again?\" He asks.");
		failed = 0;
		scanf("%c", &choice);
	}
	switch(choice){
		case '1':
			jokli_shop_potion();
			return;
		case '2':
			jokli_shop_shield();
			return;
		case '3':
			jokli_shop_sword();
			return;
		default:
			failed = 1;
			jokli_shop();
			return;
		}
}

int jokli_shop_potion(){
	check_health();
    clear_screen();
    failed = 0;
    if (failed == 0){
        if (health_potion == 0){
            printf("\"This health potion will help you if you are in a bind. That'll be 25 gold.\"\n[1]Yay\n[2]Nay");
        }
        else{
            printf("\"Oof, I just ran out of health potions, sorry.\" He says somberly. You exit the store.");
            jokli_town();
            return 1337;
        }
        printf("\nYou say:");
        scanf("%c", &choice);
        consume_newline();
    }
    else{
        failed = 0;
        printf("\"Uh, sorry, what did you say?\" He beckons you to speak up. You say again:");
        scanf("%c", &choice);
        consume_newline();
    }
    switch(toupper(choice)){
        case '1':
            clear_screen();
            if (gold > 24){
                printf("\"Good good, here's your potion of healing.\" He hands you the potion carefully, and you put it in your pouch.");
                getchar();
                gold -= 25;
                health_potion = 1;
                printf("Obtained Health Potion.");
                getchar();
                jokli_town();
                return 1337;
            }
            else{
                printf("\"Sorry sir, you don't have enough money. Also, I don't like window shoppers, so get out.\" You're taken aback, but you choose to comply and leave the store.");
                getchar();
                jokli_town();
                return 1337;
            }
        case '2':
            clear_screen();
            printf("\"Oh? Then do me a favor, and get out.\" You walk out with a blank stare on your face.");
            getchar();
            jokli_town();
            return 1337;
        default:
            failed = 1;
            jokli_shop_potion();
            return 0;
    }
}

int jokli_shop_shield(){
    check_health();
    clear_screen();
	if (failed == 0){
		printf("\"Aha, the shield. This bad boy will keep you from getting attacked to begin with. That'll be 40 gold.\"\n[Y] I'll take it!\n[N] Nah.");
		printf("\nYour Choice: ");
		scanf("%c", &choice);
		consume_newline();
	}
    else{
        printf("\"Uh, excuse me? What's that supposed to mean?\" You rephrase: ");
        scanf("%c", &choice);
        consume_newline();
    }
    switch(toupper(choice)){
        case 'Y':
            if (gold > 39){
                clear_screen();
                printf("\"Okay sir, here is your brand new shield!\" You equip the shield.");
                getchar();
                printf("You gain 10 endurance!");
                getchar();
                gold -= 40;
                endurance += 10;
		shield = 1;
                jokli_town();
                return 0;
            }
            else{
                printf("He looks unamused, and then points towards the door. You leave with your head down.");
                getchar();
                jokli_town();
                return 0;
            }
        case 'N':
            printf("\"Bah, i'm never going to sell this piece of sh- Oh well, if you change your mind, it'll be here.\" You go back to the town plaza.");
            getchar();
            jokli_town();
            return 0;
        default:
            failed = 1;
            jokli_shop_shield();
            return 0;
    }
}

int jokli_shop_sword(){
	check_health();
	clear_screen();
	if (failed == 0){
		printf("\"Oh ho, looking for some offense, eh? This guy can slice through even the toughest steel\" He says, but you think you hear him mumble something under his breath about how it can't actually do that. \"That will be 50 gold, sir.\"\n[Y]Yes\n[N]No\nChoice: ");
		scanf("%c", &choice);
		consume_newline();
	}
	else{
		clear_screen();
		printf("\"Uhh, speak up I couldn't hear you\" He says, obviously annoyed.\n[Y]Yes\n[N]No\nChoice: ");
		scanf("%c", &choice);
		consume_newline();
	}
	switch(toupper(choice)){
	case 'Y':
		clear_screen();
		if (gold > 49 && sword == 0){
			printf("He hands you the sword carefully, and you hand him the 50 gold. You gain 10 strength!");
			getchar();
			gold -= 50;
			strength += 10;
			sword = 1;
			jokli_town();
			return 0;
		}
		else if (sword == 1){
			clear_screen();
			printf("\"Hmm, seems like you have the exact same kind of sword, be kinda pointless to sell it to you\" He says. What kind of merchant doesn't want to sell stuff anyways? You leave the shop.");
			getchar();
			jokli_town();
			return 0;
		}
		else{
			printf("You look into your wallet, only to find you don't have enough gold to cover the cost. While the shopkeeper is prepping the sword, you decide to just simply leave rather than telling the shopkeeper that you don't have enough money.");
			getchar();
			jokli_town();
			return 0;
		}
	case 'N':
		clear_screen();
		printf("You decide you don't want to buy it, much to the shopkeeper's dismay and you leave the store.");
		getchar();
		jokli_town();
		return 0;
	default:
		failed = 1;
		jokli_shop_sword();
		return 0;
	}
	return 1337;
}

//Scout perk usage in jokli.
//Also handles horus' shop
void jokli_scout(){
	check_health();
 	if (failed == 0){
		if (showing_stats == 0){
			if (knowledge_of_huros == 0){
				clear_screen();
				printf("\nYou start exploring the town and quickly get a feel for where most things are. While exploring you find a weird shop called \"Horus' Shop of Goofs\" You enter the store out of curiosity, and look around. The shelves are full of... well... goofs. Like whoope cushions and what you think is a stink bomb. A man comes from the back room, most likely the owner. \"Oh wow, a customer! Welcome, welcome! Take a look around and tell if anything catches your eye!\" He says enthusatically. You look around and...");
				getchar();
			}
			if (knowledge_of_huros == 1){
				clear_screen();
				printf("You walk back into Huros' Shop of Goofs. Huros is behind the store's counter and quickly see's you. \"Hey! Welcome back! Just tell me if anything interests you!\"");
				getchar();
			}
			knowledge_of_huros = 1;
			printf("\n[0]Show Stats\n[1]The whoopie cushion catches your eye.\n[2]A small confetti cannon catches your attention.\n[3]The stink bomb looks tempting.\n[4]Nothing catches your attention\nEnter Number: ");
			scanf("%c", &choice);
			consume_newline();

		}
		else{
			showing_stats = 0;
			printf("Enter Number: ");
			scanf("%c", &choice);
			consume_newline();
		}
	}
	else{
		failed = 0;
		printf("You stare blankly until you remember you were supposed to be shopping: ");
		scanf("%c", &choice);
		consume_newline();
	}
	switch(choice){
		case '0':
			show_stats();
			jokli_scout();
		case '1':
			if (whoopie_cushion == 0){
				whoopie_horus();
				return;
			}
			else{
				printf("\"Ooo, sorry. I'm all out of whoopie cushions, you got my last one.\" He says cheerfully, as usual.");
				getchar();
				failed = 1;
				jokli_scout();
				return;
			}
		case '2':
			if (confetti_cannon == 0){
				confetti_horus();
				return;
			}
			else{
				printf("\"Awww. Unforunately i'm all out of confetti cannons. You got my last one. But I have plenty of other stuff for sale!\" He proceeds to give you the biggest smile you've ever seen. Yikes.");
				getchar();
				failed = 1;
				jokli_scout();
				return;
			}
		case '3':
			if (stink_bomb == 0){
				stink_bomb_horus();
				return;
			}
			else{
				clear_screen();
				printf("\"Well, unforunately you got my last stink bomb. If I had another i'd sell it to you in a heart beat.\" He laughs as if he told a joke, but there was no joke. Weird.");
				getchar();
				failed = 1;
				jokli_scout();
		case '4':
			clear_screen();
			printf("\"Oh... really? Oh well, if you ever want any of my fine products feel free to come back\" You think you saw a little bit of rage in his eyes, best to leave.");
			getchar();
			jokli_town();
			return;
		}
	}
}

//Whoopie cushion from huros transaction.
void whoopie_horus(){
	check_health();
	if (failed == 0){
		if (showing_stats == 0){
			clear_screen();
			printf("\"Ah, the whoopie cushion. But that isn't just any old whoopie cushion, its specially designed to change to whatever color surface you put it on.\" He demostrates this by putting it on the counter, and sure enough it turns into same color as the counter. He looks back up to you. \"20 bucks.\"\n");
			printf("[S]Show Stats\n[Y]You got yourself a deal!\n[N]What? That's crazy.\nEnter Choice: ");
			scanf("%c", &choice);
			consume_newline();
		}
		else{
			showing_stats = 0;
			printf("Enter Number: ");
			scanf("%c", &choice);
			consume_newline();
		}
	}
	else{
		failed = 0;
		printf("His face goes from a smile to a serious look. \"Deal... or no deal. Out with it!\" Seesh.\nYou say: ");
		scanf("%c", &choice);
		consume_newline();
	}
	switch(toupper(choice)){
		case 'Y':
			if (gold < 20){
				clear_screen();
				printf("\"What? You don't have enough money to pay for this. OUT, GET OUT OF MY SHOP!\" You've never seen a merchant so angry, and you quickly run out of the store.");
				getchar();
				jokli_town();
				return;
			}
			else{
				clear_screen();
				printf("\"He he, I knew you would.\" He hands you the whoopie cushion. With whoopie cushion in hand, you leave the store.");
				getchar();
				printf("You got a whoopie cushion!");
				getchar();
				gold -= 20;
				whoopie_cushion = 1;
				jokli_town();
				return;
			}
		case 'N':
			clear_screen();
			printf("He freezes, and then starts to violenty shake. You swear you see steam coming out of his ears. \"GET OUT, I DON'T LIKE WINDOW SHOPPERS! OUT, OUT!\" You get the hell out of there before he tries to kill you!");
			getchar();
			jokli_town();
			return;
		case 'S':
			show_stats();
			whoopie_horus();
			return;
		default:
			failed = 1;
			whoopie_horus();
			return;
	}
}

//Confetti cannon transaction from Horus
void confetti_horus(){
	check_health();
	if (failed == 0){
		if (showing_stats == 0){
			clear_screen();
			printf("\"Oh ho ho, the confetti cannon! This has many uses, such as maybe shooting someone in the face with it.\" That was... oddly specific. \"25 bucks.\" He says. Yay or Nay?\n[S]Show Stats\n[Y]Yay!\n[N]Nay!\nEnter Choice: ");
			scanf("%c", &choice);
			consume_newline();
		}
		else{
			showing_stats = 0;
			printf("Enter Choice: ");
			scanf("%c", &choice);
			consume_newline();
		}
	}
	else{
		failed = 0;
		printf("He looks at you like you're stupid.\"Do you want it or not?!\" He says almost yelling.\n Enter Choice: ");
		scanf("%c", &choice);
		consume_newline();
	}
	switch(toupper(choice)){
		case 'Y':
			if (gold < 25){
				clear_screen();
				printf("\"What? You don't have enough money to pay for this. OUT, GET OUT OF MY SHOP!\" You've never seen a merchant so angry, and you quickly run out of the store.");
				getchar();
				jokli_town();
			}
			else{
				clear_screen();
				printf("\"Good good, I knew you'd want it. Here ya go.\" He quickly takes the money out of your hand, and just as quickly hands you the confetti cannon.\n");
				getchar();
				printf("You got a confetti cannon!");
				getchar();
				confetti_cannon = 1;
				gold -= 25;
				jokli_town();
				return;
			}
		case 'N':
			clear_screen();
			printf("He freezes, and then starts to violenty shake. You swear you see steam coming out of his ears. \"GET OUT, I DON'T LIKE WINDOW SHOPPERS! OUT, OUT!\" You get the hell out of there before he tries to kill you!");
			getchar();
			jokli_town();
			return;
		case 'S':
			show_stats();
			confetti_horus();
			return;
		default:
			failed = 1;
			confetti_horus();
			return;
	}
}

//Stink bomb transaction from horus
void stink_bomb_horus(){
	check_health();
	if (failed == 0){
		if (showing_stats == 0){
			clear_screen();
			printf("\"Ha ha! The classic stink bomb! Throw this at anyone and the smell will overwhelm them. The smell is so bad, it might even kill them!\" Somehow, you get the feeling that last part isn't a joke. \"50 bucks\" Holy crap! That's expensive!\n[S]Show Stats\n[Y]Alright, here's your money.\n[N]50 bucks? No way!\nEnter Choice: ");
			scanf("%c", &choice);
			consume_newline();
		}
		else{
			showing_stats = 0;
			printf("Enter Choice: ");
			scanf("%c", &choice);
			consume_newline();
		}
	}
	else{
		printf("\"Are you trying to haggle me? Because I don't haggle. Take the deal or GET OUT!\" Seesh.\nEnter Choice: ");
		scanf("%c", &choice);
		consume_newline();
	}
	switch(toupper(choice)){
		case 'Y':
			if (gold < 50){
				clear_screen();
				printf("\"What? You don't have enough money to pay for this. OUT, GET OUT OF MY SHOP!\" You've never seen a merchant so angry, and you quickly run out of the store.");
				getchar();
				jokli_town();
				return;
			}
			else{
				printf("\"Heh heh, here ya go. Enjoy your stink bomb.\" He snatches the money quickly and gives you the stinkbomb, better becareful with this.");
				getchar();
				printf("You got a stink bomb!");
				getchar();
				gold -= 50;
				stink_bomb = 1;
				jokli_town();
				return;
			}
		case 'N':
			clear_screen();
			printf("He freezes, and then starts to violenty shake. You swear you see steam coming out of his ears. \"GET OUT, I DON'T LIKE WINDOW SHOPPERS! OUT, OUT!\" You get the hell out of there before he tries to kill you!");
			getchar();
			jokli_town();
			return;
		case 'S':
			show_stats();
			stink_bomb_horus();
			return;
		default:
			failed = 1;
			stink_bomb_horus();
			return;
	}
}
