#ifndef MAIN_VARIABLES_H
#define MAIN_VARIABLES_H

//Functions
	//Gameplay
		void consume_newline();
		void clear_screen();
		void show_stats();
		void check_health();
	//Debug
		void debug_choice();
        void debug_gold();
        void debug_area();
        void debug_attributes();
	//Character Creation
		void childhood();
		void character_creation();
		void childhood_question_one();
		void childhood_question_two();
		void childhood_question_three();
		void childhood_over();
	//Jokli
		void prologue_();
		void jokli_scout();
		void jokli_shop();
		void jokli_tavern();
		void talking_to_locals_jokli();
		void jokli_town();
		//Horus Related
			void whoopie_horus();
			void confetti_horus();
			void stink_bomb_horus();
		//Jokli General Store related
			int jokli_shop_potion();
			int jokli_shop_shield();
			int jokli_shop_sword();

//Variables
	//Gameplay
		extern int distance;
		extern int showing_stats;
		extern char choice;
	//Environment Variables
		extern int failed;
	//Character
	extern char name[20];
		//Attributes
			extern int health;
			extern int speech;
			extern int speed;
			extern int strength;
			extern int agility;
			extern int endurance;
			extern int intelligence;
			extern int gold;
	//Inventory
		//From Horus' Goof Shop in Jokli
			extern int whoopie_cushion;
			extern int confetti_cannon;
			extern int stink_bomb;
            //From Generic Shops
            extern int health_potion;
            extern int sword;
            extern int shield;

	//Perks
		//Character Creation Perks
			extern int crafty;
			extern int martial_artist;
			extern int scout;
			extern int peddler;
			extern int magic_user;
			extern int wild_world;
	//Jokli
		//Local related
			extern int talked_to_jokli_locals;
		//Kakles related
			extern int knowledge_of_kakles;
			extern int been_to_kakles_jokli;
		//Tavern Related
			extern int	been_to_tavern_jokli;
		//Shop related
			extern int been_to_store_jokli;
		//Horus Related.
			extern int already_scouted_jokli;
			extern int knowledge_of_huros;
#endif
