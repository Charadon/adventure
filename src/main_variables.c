#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <main_variables.h>
//Debug
    int debug_mode = 1;
//Gameplay
	int distance = 5;
	int showing_stats = 0;
	char choice;
	//Environment Variables
		int failed = 0;
//Character
char name[20];
	//Attributes
		int health = 100;
		int speech = 5;
		int speed = 5;
		int strength = 5;
		int agility = 5;
		int endurance = 5;
		int intelligence = 5;
		int gold = 25;
	//Inventory
		//From Horus' Goof Shop in Jokli
			int whoopie_cushion = 0;
			int confetti_cannon = 0;
			int stink_bomb = 0;
            //From Generic Shops
            int health_potion = 0;
            int sword = 0;
            int shield = 0;
	//Perks
		//Character Creation Perks
			int crafty = 0;
			int martial_artist = 0;
			int scout = 0;
			int peddler = 0;
			int magic_user = 0;
			int wild_world = 0;
	//Jokli
	//Local related
		int talked_to_jokli_locals = 0;
		//Kakles related
			int knowledge_of_kakles = 0;
			int been_to_kakles_jokli = 0;
		//Tavern Related
			int	been_to_tavern_jokli = 0;
		//Shop related
			int been_to_store_jokli = 0;
		//Horus Related.
			int already_scouted_jokli = 0;
			int knowledge_of_huros = 0;
